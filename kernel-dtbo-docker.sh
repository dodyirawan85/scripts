#!/usr/bin/env bash
#
# Copyright (C) 2018-2019 Rama Bondan Prakoso (rama982)
#
# Docker Kernel Build Script

# TELEGRAM START
git clone --depth=1 https://github.com/fabianonline/telegram.sh telegram

TELEGRAM=telegram/telegram

tg_channelcast() {
  "${TELEGRAM}" -f "$(echo "$ZIP_DIR"/*.zip)" \
  -t $TELEGRAM_TOKEN \
  -c $CHAT_ID -H \
      "$(
          for POST in "${@}"; do
              echo "${POST}"
          done
      )"
}
# TELEGRAM END

# Main environtment
BRANCH="$(git rev-parse --abbrev-ref HEAD)"
KERNEL_DIR=$(pwd)
PARENT_DIR="$(dirname "$KERNEL_DIR")"
KERN_IMG=$KERNEL_DIR/out/arch/arm64/boot/Image.gz-dtb
DTBO_IMG=$KERNEL_DIR/out/arch/arm64/boot/dtbo.img

git submodule update --init --recursive
git clone https://github.com/dodyirawan85/AnyKernel3.git -b inc-dtbo
if [ "$TC" = "clang" ]; then
    git clone https://github.com/kdrag0n/proton-clang.git compiler --depth=1
    export PATH="$PWD/compiler/bin:$PATH"
    export KBUILD_COMPILER_STRING="$(${PWD}/compiler/bin/clang --version | head -n 1 | perl -pe 's/\(http.*?\)//gs' | sed -e 's/  */ /g' -e 's/[[:space:]]*$//')"
    sed -i '/CONFIG_CC_STACKPROTECTOR_STRONG=y/d' arch/arm64/configs/${DEFCONFIG}
else
    git clone https://github.com/silont-project/aarch64-elf-gcc.git -b arm64/11 compiler --depth=1
    git clone https://github.com/silont-project/arm-eabi-gcc.git -b arm/10 compiler-32 --depth=1
    export CROSS_COMPILE=$PWD/compiler/bin/aarch64-elf-
    export CROSS_COMPILE_ARM32=$PWD/compiler-32/bin/arm-eabi-
fi

# Build kernel
export TZ="Asia/Jakarta"
export ARCH=arm64
export KBUILD_BUILD_USER="dodyirawan85"
export KBUILD_BUILD_HOST="docker-builds"
KBUILD_BUILD_TIMESTAMP=$(date)

if [ ${TYPE} = "DEBUG" ]; then
    echo "CONFIG_PSTORE=y" >> ${DEFCONFIG}
    echo "CONFIG_PSTORE_CONSOLE=y" >> ${DEFCONFIG}
    echo "CONFIG_PSTORE_PMSG=y" >> ${DEFCONFIG}
    echo "CONFIG_PSTORE_RAM=y" >> ${DEFCONFIG}
    echo "CONFIG_AUDIT=y" >> ${DEFCONFIG}
    echo "# CONFIG_AUDITSYSCALL is not set" >> ${DEFCONFIG}
    export CODENAME="DEBUG"
else
    export CODENAME=$(git rev-parse --abbrev-ref HEAD)
fi

build_kernel () {
    if [ "$TC" = "clang" ]; then
        build_kernel_clang
    else
        build_kernel_gcc
    fi
}

build_kernel_gcc () {
    make -j$(nproc --all) O=out
}

build_kernel_clang () {
    make -j$(nproc --all) O=out \
        ARCH=arm64 \
        CC=clang \
        CROSS_COMPILE=aarch64-linux-gnu- \
        CROSS_COMPILE_ARM32=arm-linux-gnueabi- \
        LLVM=1
}

make O=out ARCH=arm64 ${DEFCONFIG}
build_kernel
if ! [ -f "$KERN_IMG" ]; then
    tg_channelcast "<b>BuildCI report status:</b> There are build running but its error, please fix and remove this message!"
    exit 1
fi

# Make zip installer

# ENV
ZIP_DIR=$KERNEL_DIR/AnyKernel3

# Modify kernel name in anykernel
sed -i "s/ExampleKernel by osm0sis @ xda-developers/${KERNAME} ${CODENAME} by dodyirawan85 @ github.com/g" $ZIP_DIR/anykernel.sh

# Make zip
make -C $ZIP_DIR clean
cp $KERN_IMG $ZIP_DIR
cp $DTBO_IMG $ZIP_DIR
make -C $ZIP_DIR normal

KERNEL=$(cat out/.config | grep Linux/arm64 | cut -d " " -f3)
FILEPATH=$(echo "$ZIP_DIR"/*.zip)
HASH=$(git log --pretty=format:'%h' -1)
COMMIT=$(git log --pretty=format:'%h: %s' -1)
URL=$(git config --get remote.origin.url)
URL="${URL/.git/}"
tg_channelcast "<b>Latest commit:</b> <a href='$URL/commits/$HASH'>$COMMIT</a>" \
               "<b>Device:</b> $SUPPORTED" \
               "<b>Kernel:</b> $KERNEL - $CODENAME" \
               "<b>Type:</b> $TYPE" \
               "<b>Firmware:</b> $FIRMWARE" \
               "<b>sha1sum:</b> <pre>$(sha1sum "$FILEPATH" | awk '{ print $1 }')</pre>" \
               "<b>Date:</b> $KBUILD_BUILD_TIMESTAMP"
