#!/usr/bin/env bash
#
# Copyright (C) 2018-2019 Rama Bondan Prakoso (rama982)
#
# Docker Kernel Build Script

# TELEGRAM START
git clone --depth=1 https://github.com/fabianonline/telegram.sh telegram
pacman -S python2 --noconfirm
rm -rf /usr/bin/python
ln -sf /usr/bin/python2 /usr/bin/python

TELEGRAM=telegram/telegram

pushKernel() {
	curl -F document=@$(echo $ZIP_DIR/*.zip) https://api.telegram.org/bot$TELEGRAM_TOKEN/sendDocument\?chat_id="$CHANNEL_ID"
}

tg_channelcast() {
    "${TELEGRAM}" -c ${CHANNEL_ID} -H \
        "$(
            for POST in "${@}"; do
                echo "${POST}"
            done
        )"
}

# TELEGRAM END

# Main environtment
git submodule update --init --recursive
BRANCH="$(git rev-parse --abbrev-ref HEAD)"
KERNEL_DIR=$(pwd)
PARENT_DIR="$(dirname "$KERNEL_DIR")"
KERN_IMG=$KERNEL_DIR/arch/arm/boot/zImage
KERN_DTB=$KERNEL_DIR/arch/arm/boot/dtb.img
CONFIG="aosp-n-a16c3h_defconfig"
CONFIG_PATH=$KERNEL_DIR/arch/arm/configs/$CONFIG
NAME="Smartfren Andromax A16C3H / G151"
export DEVICE="A16C3H"

sed -i 's/perf/'$KERNAME'-'$DEVICE'/g' $CONFIG_PATH

wget -O linaro-4.tar.xz https://releases.linaro.org/components/toolchain/binaries/latest-4/arm-eabi/gcc-linaro-4.9.4-2017.01-x86_64_arm-eabi.tar.xz
mkdir -p $PARENT_DIR/toolchain/linaro
tar -xvf *.tar.xz -C $PARENT_DIR/toolchain/linaro --strip-components=1
git clone https://github.com/dodyirawan85/AnyKernel3.git -b master

# Build kernel
export KBUILD_BUILD_USER="いらわん"
export KBUILD_BUILD_HOST="docker"
export TZ="Asia/Jakarta"
export CROSS_COMPILE=$PARENT_DIR/toolchain/linaro/bin/arm-eabi-
export KBUILD_COMPILER_STRING="$($PARENT_DIR/toolchain/linaro/bin/arm-eabi-gcc --version | head -n 1)"
export ARCH=arm

build_kernel () {
    make -j$(nproc --all)
    scripts/dtbToolCM -2 -o $KERN_DTB -s 2048 -p scripts/dtc/ arch/arm/boot/dts/
}

make $CONFIG
build_kernel
if ! [ -a $KERN_IMG ]; then
    tg_channelcast "<b>BuildCI report status:</b> There are build running but its error, please fix and remove this message!"
    exit 1
fi

# Make zip installer

# ENV
ZIP_DIR=$KERNEL_DIR/AnyKernel3

sed -i 's/ExampleKernel by osm0sis @ xda-developer/'$KERNAME'-'$DEVICE' by dodyirawan85 @ github.com/g' $ZIP_DIR/anykernel.sh

# Make zip
make -C $ZIP_DIR clean
cp $KERN_IMG $ZIP_DIR
cp $KERN_DTB $ZIP_DIR
make -C $ZIP_DIR normal

KERNEL=$(cat .config | grep Linux/arm | cut -d " " -f3)
# tg_sendstick
pushKernel
tg_channelcast "New successfull build for device <b>$NAME</b> branch <b>$BRANCH</b> Linux version <b>$KERNEL</b> toolchain <b>${KBUILD_COMPILER_STRING}</b> at commit point <b>$(git log --pretty=format:'"%h : %s"' -1)</b>"