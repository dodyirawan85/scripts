#!/usr/bin/env bash
#
# Copyright (C) 2018-2019 Rama Bondan Prakoso (rama982)
#
# Docker Kernel Build Script

# TELEGRAM START
git clone --depth=1 https://github.com/fabianonline/telegram.sh telegram
pacman -S python2 --noconfirm

TELEGRAM=telegram/telegram

pushKernel() {
	curl -F document=@$(echo $ZIP_DIR/*.zip) https://api.telegram.org/bot$TELEGRAM_TOKEN/sendDocument\?chat_id="$CHANNEL_ID"
}

tg_channelcast() {
    "${TELEGRAM}" -c ${CHANNEL_ID} -H \
        "$(
            for POST in "${@}"; do
                echo "${POST}"
            done
        )"
}

# tg_sendstick() {
#     curl -s -X POST "https://api.telegram.org/bot$TELEGRAM_TOKEN/sendSticker" \
#         -d sticker="CAADBQADCwADfmlfEgEtqXB1SD3FFgQ" \
#         -d chat_id="$CHANNEL_ID"
# }
# TELEGRAM END

# Main environtment
git submodule update --init --recursive
BRANCH="$(git rev-parse --abbrev-ref HEAD)"
KERNEL_DIR=$(pwd)
PARENT_DIR="$(dirname "$KERNEL_DIR")"
KERN_IMG=$KERNEL_DIR/out/arch/arm64/boot/Image.gz-dtb
DTBO_IMG=$KERNEL_DIR/out/arch/arm64/boot/dtbo.img
CONFIG="RMX1851_defconfig"
CONFIG_PATH=$KERNEL_DIR/arch/arm64/configs/$CONFIG
NAME="Realme 3 Pro / RMX1851"

wget -O linaro-4.9.tar.xz https://releases.linaro.org/components/toolchain/binaries/latest-4/aarch64-linux-gnu/gcc-linaro-4.9.4-2017.01-x86_64_aarch64-linux-gnu.tar.xz
mkdir -p ../toolchain/linaro
tar -xvf *.tar.xz -C ../toolchain/linaro --strip-components=1
git clone https://github.com/dodyirawan85/AnyKernel3.git -b rmx1851

# Build kernel
export PATH="$PARENT_DIR/toolchain/clang/bin:$PATH"
export KBUILD_BUILD_USER="いらわん"
export KBUILD_BUILD_HOST="docker"
export TZ="Asia/Jakarta"
export CROSS_COMPILE=$PARENT_DIR/toolchain/linaro/bin/aarch64-linux-gnu-
export KBUILD_COMPILER_STRING="$($PARENT_DIR/toolchain/linaro/bin/aarch64-linux-gnu-gcc --version | head -n 1)"
export OPPO_TARGET_DEVICE=18621
export VENDOR_EDIT=1
export MSM_18621=1
export IS_PROJECT_18621=1
export OPPO_18621=1
export DEVICE="RMX1851"
export ARCH=arm64

build_kernel () {
    make -j$(nproc --all) O=out
}

sed -i 's/perf/'$KERNAME'-'$DEVICE'/g' $CONFIG_PATH

make O=out $CONFIG
build_kernel
if ! [ -a $KERN_IMG ]; then
    tg_channelcast "<b>BuildCI report status:</b> There are build running but its error, please fix and remove this message!"
    exit 1
fi

# Make zip installer

# ENV
ZIP_DIR=$KERNEL_DIR/AnyKernel3

sed -i 's/ExampleKernel by osm0sis @ xda-developers/'$KERNAME' by dodyirawan85 @ github.com/g' $ZIP_DIR/anykernel.sh

make -C $ZIP_DIR clean
cp $KERN_IMG $ZIP_DIR
cp $DTBO_IMG $ZIP_DIR
make -C $ZIP_DIR normal

KERNEL=$(cat out/.config | grep Linux/arm64 | cut -d " " -f3)
# tg_sendstick
pushKernel
tg_channelcast "New successfull build for device <b>$NAME</b> branch <b>$BRANCH</b> Linux version <b>$KERNEL</b> toolchain <b>${KBUILD_COMPILER_STRING}</b> at commit point <b>$(git log --pretty=format:'"%h : %s"' -1)</b>"