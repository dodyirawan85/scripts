#!/usr/bin/env bash
#
# Copyright (C) 2018-2019 Rama Bondan Prakoso (rama982)
#
# Docker Kernel Build Script

# TELEGRAM START
git clone --depth=1 https://github.com/fabianonline/telegram.sh telegram

TELEGRAM=telegram/telegram

pushKernel() {
	curl -F document=@$(echo $ZIP_DIR/*.zip) https://api.telegram.org/bot$TELEGRAM_TOKEN/sendDocument\?chat_id="$CHANNEL_ID"
}

tg_channelcast() {
    "${TELEGRAM}" -c ${CHANNEL_ID} -H \
        "$(
            for POST in "${@}"; do
                echo "${POST}"
            done
        )"
}

# tg_sendstick() {
#     curl -s -X POST "https://api.telegram.org/bot$TELEGRAM_TOKEN/sendSticker" \
#         -d sticker="CAADBQADCwADfmlfEgEtqXB1SD3FFgQ" \
#         -d chat_id="$CHANNEL_ID"
# }
# TELEGRAM END

# Main environtment
BRANCH="$(git rev-parse --abbrev-ref HEAD)"
KERNEL_DIR=$(pwd)
PARENT_DIR="$(dirname "$KERNEL_DIR")"
KERN_IMG=$KERNEL_DIR/arch/arm/boot/zImage
CONFIG_PATH=$KERNEL_DIR/arch/arm/configs/$CONFIG

apt -y install xz-utils
wget -O linaro-4.9.tar.xz https://releases.linaro.org/components/toolchain/binaries/latest-4/arm-eabi/gcc-linaro-4.9.4-2017.01-x86_64_arm-eabi.tar.xz
tar -xvf linaro-4.9.tar.xz
git clone https://github.com/rama982/AnyKernel3 -b master

# Build kernel
export KBUILD_BUILD_USER="irawans"
export KBUILD_BUILD_HOST="docker"
export TZ="Asia/Jakarta"
KBUILD_COMPILER_STRING="$(gcc-linaro-4.9.4-2017.01-x86_64_arm-eabi/bin/arm-eabi-gcc --version | head -n 1)"

build_kernel () {
    make -j$(nproc --all) \
        ARCH=arm \
        CROSS_COMPILE=gcc-linaro-4.9.4-2017.01-x86_64_arm-eabi/bin/arm-eabi-
}

make ARCH=arm $CONFIG
build_kernel
if ! [ -a $KERN_IMG ]; then
    tg_channelcast "<b>BuildCI report status:</b> There are build running but its error, please fix and remove this message!"
    exit 1
fi

# Make zip installer

# ENV
ZIP_DIR=$KERNEL_DIR/AnyKernel3

# Make zip
make -C $ZIP_DIR clean
cp $KERN_IMG $ZIP_DIR
make -C $ZIP_DIR normal

NAME="Andromax AD688G / C2 Old"

KERNEL=$(cat .config | grep Linux/arm | cut -d " " -f3)
# tg_sendstick
pushKernel
tg_channelcast "New successfull build for device <b>$NAME</b> <br>branch <b>$BRANCH</b> <br>Linux version <b>$KERNEL</b> <br>toolchain <b>${KBUILD_COMPILER_STRING}</b> <br>at commit point <b>$(git log --pretty=format:'"%h : %s"' -1)</b>"